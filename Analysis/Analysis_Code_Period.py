#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 19:12:41 2017

@author: gee
"""

' BEWARE: your directory must be phy571_project '

import sys
sys.path.append('Design')
import numpy as np
import GPE_FFT_2D_Library as lib
from numpy.fft import fft, ifft, fftfreq


# define  the variables
L = 10
J = 256
T = 200
N= 100001
N_0= 1000
V_1, V_2= 1,1
eta= 1
x_pts, dx = np.linspace(-L/2, L/2, J, retstep=True)
y_pts, dy = np.linspace(-L/2, L/2, J, retstep=True)
xx,yy = np.meshgrid(x_pts, y_pts)

t_acquisition = 3000

# just to remember the set of values for which we have computed the period
eta_values = [-20, -5, -2, -1, 1, 2, 5, 20]
xshift_values = [0.5, 1, 2]
a_values = [0.1, 0.5, 1, 2, 5, 10]

eta = 20
xshift = 1
a= 1

# Loading the result from the right path CAREFUL: Your directory must be phy571_project
result = abs(lib.load('Production/period_record_result/period_record_eta=' + str(eta) + '_xshift='
                      + str(xshift) + '_a='+str(a)))**2

# calculating the derivative to eliminate the offset of the signal
result = np.diff(result / (10*lib.dt))

# adjusting the time scale (in the record function we take only one dt out of ten)
#t_pts_adjusted = lib.adjust_time(result, lib.t_pts)

t_pts_adjusted = []
t_pts_new = np.arange(0, 400000, lib.dt)
# taking only one in ten points
for i in range(len(t_pts_new) // 10):
   t_pts_adjusted.append(t_pts_new[10*i])
t_pts_adjusted = t_pts_adjusted[:len(result)]
#  plotting the signal in dt units
lib.plot_1d(t_pts_adjusted / (lib.dt), result,'time', 'projection of vortex on itself',
            'period of the vortex for eta = '+ str(eta) + ' xshift = ' + str(xshift) + 
                   ' a =' + str(a))

# Periodizing the signal
window = np.hanning(len(result))
result *= window

# Computing the Fourier transform (and adding zeroes to the signal for accuracy)
fft_result = abs((fft(result, n = len(result)*4)))
kt = 2 * np.pi * fftfreq(len(fft_result), d= 10*lib.dt)

# Plotting the Fourier transform
lib.plot_1d(kt, fft_result,'frequency', 'intensity', 'specter of the vortex for eta = '+ str(eta) + ' xshift = ' + str(xshift) + 
                   ' a =' + str(a) )

# Extraction of the frequency

print ('the precession pulsation for eta = '+ str(eta) + ' xshift = ' + str(xshift) + 
                   ' a =' + str(a) +' is :'+str(kt[fft_result.argmax()]))


