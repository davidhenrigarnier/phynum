#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 19:33:00 2017

@author: gee
"""

import sys
sys.path.append('Design')
import numpy as np
import GPE_FFT_2D_Library as lib

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# define the variables
eta_values = [20]
xshift_values = np.linspace(0.5, 1.2, 10)
xshift_values = xshift_values[xshift_values< 1.2]

a_values = [1]

# initialize the record
regression_record= []

for eta in eta_values :
    for xshift in xshift_values :
        for a in a_values :
            # compute the pulsation for different xshift and add it to the record
            regression_record.append(lib.find_pulsation(lib.t_pts, lib.dt ,eta, xshift, a))
            
#lib.plot_1d(xshift_values_reg, regression_record, 'xshift', 'pulsation', ' pulsation as a function of xshift')

# we define the function that we went to be fitted with
def fitting_func(x, a, b):
    
    return a * 1/(1-(x/b)**2)



# call the curve_fit that returns the function and a array of the errors, with reasonable boundaries (xshift < b) 
    
popt, pcov = curve_fit(fitting_func, xshift_values, regression_record, bounds=([-1,2], [3., 10]))

# plot the data
plt.plot(xshift_values, regression_record, 'o', label='data')

# plot on the same figure the optimal function
plt.plot(xshift_values, fitting_func(xshift_values, *popt), 'r-',
         label='fit: a=%5.3f, b=%5.3f' % tuple(popt))

plt.xlabel('xshift values')
plt.ylabel('pulsation')
plt.legend()
plt.show()
print (pcov)