
# coding: utf-8

# In[43]:


#get_ipython().magic('matplotlib notebook')

from math import factorial
from scipy.special import hermite
import numpy as np
from numpy import exp, pi, sqrt, linspace
from numpy.fft import fft, fft2, ifft2, fftfreq
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


# In[21]:


' we define the parameters  T_op2, V_op2 V_1 V_2, and  kx ky , eta without losses'

# Spatial grids
L, J = 10, 256

x_pts, dx = linspace(-L/2, L/2, J, retstep=True)
y_pts, dy = linspace(-L/2, L/2, J, retstep=True)
xx,yy = np.meshgrid(x_pts, y_pts)


# temporal grid

T, N = 200, 100001
t_pts, dt = np.linspace(0., T, N, retstep=True)

# k space grids

kx = 2 * pi * fftfreq(len(x_pts), d=dx)
ky = 2 * pi * fftfreq(len(y_pts), d=dy)
kxx, kyy = np.meshgrid(kx, ky)

# Potentials and non linearity
V_1 = 1
V_2 = 1
eta = 1


# In[22]:


#def functions f_conjugate and f_v 

def f_conj(Psi):
    """ returns the squared norm of Psi """
    f_conj = np.conjugate(Psi) * Psi
    return abs(f_conj)


def f_v(x_pts, V):
    """ defines the 1D potential """
    f_v= (x_pts) * (x_pts) * (V)
    return f_v




def f_v2(xx, V_1, yy, V_2):
    """ defines the 2D potential """
    f_v2= (f_v(xx, V_1) + f_v(yy,V_2))
    return f_v2







# In[23]:


' we define T_op2, V_op2 V_1 V_2, and  kx ky , eta without losses'



def initiate_op2(xx, yy, kxx, kyy, dt, V_1, V_2):
   """ This function initiate the operators needed for the numerical solution """ 
   T_op2 = exp(-1j * (kxx**2 + kyy**2) * dt / 2)
   V_op2 = exp(-1j * f_v2(xx, V_1, yy, V_2) * dt / 2)
   
   return  T_op2, V_op2


def linear_step2(Psi, T_op2, V_op2):
    
    # kinetic
    Psi[:] = fft2(ifft2(Psi) * T_op2)
    
    # potential
    Psi *= V_op2
    
    return Psi


def nonlinear_step2(Psi, T_op2, V_op2, dt, eta):
   """ function that modifies in place Psi and makes it make one linear step AND one non linear step """

   # linear step
   linear_step2(Psi, T_op2, V_op2)

   # nonlinear
   Psi *= exp(-1j * eta * abs(Psi)**2 * dt)
  
   return Psi




def N_steps_in_place(Psi, xx, yy, kxx, kyy, dt, V_1, V_2, N, eta):
    """ This function modifies in place the wavefunction Psi and makes it advance N steps of time """
    # We initiate the operators
    T_op2, V_op2 = initiate_op2(xx, yy, kxx, kyy, dt, V_1, V_2)
    
    for i in range (1,N):
        nonlinear_step2(Psi, T_op2, V_op2, dt, eta)
        # Normalization after each step
        Psi /= sqrt((f_conj(Psi).sum()*dx*dy))
        
    return Psi

    



# In[24]:


# we iterate the process 
def N_steps_non_linear2(Psi_0, xx, yy, kxx, kyy, dt, V_1, V_2, Psi_rec, N, eta):
    Psi = Psi_0.copy()
    Psi_rec.append(Psi)
    # We initiate the operators
    T_op2, V_op2 = initiate_op2(xx, yy, kxx, kyy, dt, V_1, V_2)
    
    for i in range (1,N):
        Psi_new = nonlinear_step2(Psi, T_op2, V_op2, dt, eta)
        Psi = Psi_new.copy()
        Psi_rec.append(Psi)
        
    return Psi_rec


        


# In[25]:


'Test functions'

#bidimensional gaussian
def gaussian2d(xx, yy, w=1):
    """ 2D gaussian """
    
    
    return exp(-(xx**2 + yy**2) / w**2) + 0j


def QHO(n, xx, xshift=0, t=0):
    """ Quantum Harmonic oscillator wavefunctions """
    
    
    E= n + 0,5
    coef = 1 / sqrt(2**n * factorial(n)) * (1 / pi)**(1/4)
    hermite1 = hermite(n)
    xs = xx- xshift
    
    return coef * exp(-xs**2 / 2 - 1j*E*t) * hermite1(xs)

def QHO2(n, m, xx, yy, xshift=0, yshift=0, t=0):
    return QHO(n, xx, xshift)*QHO(m, yy, yshift)





# In[28]:


# Find Ground State: we have to normalize after each iteration 

def nonlinear_step_normalized2(Psi, T_op2, V_op2, dt, eta):
    Psi_gi = nonlinear_step2(Psi, T_op2, V_op2, dt, eta)
    Psi_g = Psi_gi / sqrt((f_conj(Psi_gi).sum()*dx*dy))
    
    return Psi_g

def N_nonlinear_steps_normalized2(Psi_0, xx, yy, kxx, kyy, dt, V_1, V_2, Psi_rec, N, eta):
    Psi = Psi_0.copy()
    Psi_rec.append(Psi)
    # We initiate the operators
    T_op2, V_op2 = initiate_op2(xx, yy, kxx, kyy, dt, V_1, V_2)
    
    for i in range (1,N):
        Psi_new = nonlinear_step_normalized2(Psi, T_op2, V_op2, dt, eta)
        Psi = Psi_new.copy()
        Psi_rec.append(Psi)
        
    return Psi_rec
                   


def find_groundstate(L, J, T, N, N_0, V_1, V_2, eta, name):
    """ function that creates twofiles name_real and name_imag with the groundstate of the function """
   
    
    # Spatial grids

    x_pts, dx = linspace(-L/2, L/2, J, retstep=True)
    y_pts, dy = linspace(-L/2, L/2, J, retstep=True)
    xx,yy = np.meshgrid(x_pts, y_pts)

    # temporal grid

    t_pts, dt = np.linspace(0., T, N, retstep=True)

    # k space grids

    kx = 2 * pi * fftfreq(len(x_pts), d=dx)
    ky = 2 * pi * fftfreq(len(y_pts), d=dy)
    kxx, kyy = np.meshgrid(kx, ky)

    
    # We found the ground state with a gaussian
    Psi_0 = gaussian2d(xx, yy)
    Psi_rec = []
    
    # We calculate the groundstate and we save it 
    N_nonlinear_steps_normalized2(Psi_0, xx, yy, kxx, kyy, -1j*dt, V_1, V_2, Psi_rec, N_0, eta)
    name = name + '_eta=' + str(eta)
    np.savetxt(name+'_real', Psi_rec[-1].real)
    np.savetxt(name+'_imag', Psi_rec[-1].imag)

# In[1]:


' Graphic Treatment : 1d, pcolormesh and 3d surface'



def plot_1d(x, y1, abscissa, result, title):
    """  1d plot function """
    fig, ax= plt.subplots()
    
    ax.plot(x, y1, label='signal')
    
    ax.set_xlabel(abscissa ) 
    ax.set_ylabel(result)
    ax.set_title(title)
    ax.legend()
    fig.show()


def plot_pcolormesh(xx, yy, sol_g):
   """  Plot the colormesh """ 
   fig, ax = plt.subplots()
   ax.pcolormesh(xx,yy,sol_g, cmap='viridis')
   fig.show()


# Plot the surface
def plot_surface(xx, yy, sol_g):

   fig = plt.figure()
   ax = fig.gca(projection='3d')


   # Plot the surface.
   surf = ax.plot_surface(xx, yy, sol_g, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

   # Customize the z axis.
   ax.set_zlim(-1, 1)
   ax.zaxis.set_major_locator(LinearLocator(10))
   ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

   # Add a color bar which maps values to colors.
   fig.colorbar(surf, shrink=0.5, aspect=5)
   plt.show()


# In[ ]:


'Loading module'

def load(name):
    """ load reconstructs a complex array with two arrays saved under name+_real and name+_imag """
    return (np.loadtxt(name +'_real') + 1j*np.loadtxt(name + '_imag'))


# In []:
' Vortex Functions'



def add_vortex(ground_state, xx, yy, xshift, a):
    """add_vortex adds a vortex with a horizontal shift to the ground state """
    xs = xx-xshift
    rho = np.sqrt(xs*xs + yy*yy)
    f = (rho / a) / np.sqrt(1 + (rho/a)**2)
    theta = np.arctan2(yy, xs)
    
    return ground_state * f * np.exp(1j*theta)



from matplotlib.animation import FuncAnimation
def animate_vortex(Psi, xx, yy, kxx, kyy, dt, V_1, V_2, eta, xshift, a):
    """  animate_vortex receives a function and returns a animation of its evolution """
    
    # you can get the window limits from from xx or yy

    fig, ax = plt.subplots()
    norm2 = f_conj(Psi)

    im = ax.imshow(norm2, extent=(xx.min(), xx.max(), yy.min(), yy.max()))

    ax.set_title('t = 0')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    fig.colorbar(im)
    fig.tight_layout()

    # extra parameters can be passed into animate using fargs (below)
    def animate(i, ax, im):
        N_0 = 10
        
        # make many steps for each frame
        N_steps_in_place(Psi, xx, yy, kxx, kyy, dt, V_1, V_2, N_0, eta)   

        norm2 = f_conj(Psi)
        im.set_data(norm2)
        ax.set_title('i = {}, max-value = {:1.2f}'.format(i*N_0, norm2.max()) + ' for eta = '+ str(eta) + ' xshift = ' + str(xshift) + 
                   ' a =' + str(a))
        return im, 
    # FunctionAnimation has an argument called fargs, which specifies
    # extra arguments to be pased to the animate function
    anim = FuncAnimation(fig, animate, interval=10, fargs=(ax, im))

    plt.show()





'Result treatment functions'
    

def adjust_time(result, t_pts, dt):
    """ this function adapts the time scale to the time scale of the periodic function that contains the results"""
    
    t_pts_adjusted = []
    if (len(result) < len(t_pts)) :
        # taking only one in ten points
        for i in range(len(t_pts) // 10):
            t_pts_adjusted.append(t_pts[10*i])
        
        # we truncate the values to match the shape of result
        return t_pts_adjusted[:len(result)]
    else :
        
        t_pts_new = np.arange(0, 400000, dt)
        # taking only one in ten points
        for i in range(len(t_pts_new) // 10):
           t_pts_adjusted.append(t_pts_new[10*i])
        # readapt the scale   
           t_pts_adjusted = t_pts_adjusted[:len(result)]
        return t_pts_adjusted




def period(t_acquisition, vortex, xx, yy, kxx, kyy, dx, dy, dt, V_1, V_2, eta, xshift, a):
     """ the function period creates a 1d array with the values of <vortex/ vortex_0> at different times and saves it """
     
     
     # we initiate the period record 
     period_record = []
     vortex_0 = vortex.copy()
     # first element = the scalar product with itself
     period_record.append((np.conjugate(vortex_0) * vortex_0 * dx * dy).sum())
     
     for i in range (t_acquisition):
         # We advance the vortex of 10 dt
         N_steps_in_place(vortex, xx, yy, kxx, kyy, dt, V_1, V_2, 10, eta)
         # we calculate its scalar product with the original vortex and add it to the record
         period_record.append((np.conjugate(vortex) * vortex_0 * dx * dy).sum())
     
     # the period returns a list so we have to turn it into an array
     period_record = np.asarray(period_record)
    
     # define its name
     name = 'period_record' +'_eta='+ str(eta) + '_xshift=' + str(xshift) + '_a=' + str(a)
     # save it in a text file. careful : duration = 10*t_acquisition*dt 
     np.savetxt(name + '_real',period_record.real)
     np.savetxt(name + '_imag',period_record.imag) 
     
     
def find_pulsation(t_pts, dt, eta, xshift, a):
        """ function that from a set of variables eta xshift and loads the pre computed solution
        in the Production/period_record_result file , treats the signal and return the period. """
     
     
        # Loading the result from the right path CAREFUL: Your directory must be phy571_project
        result = abs(load('Production/period_record_result/period_record_eta=' + str(eta) + '_xshift='+ str(xshift) +'_a='+str(a)))**2
        
        # calculating the derivative to eliminate the offset of the signal
        result = np.diff(result / (10*dt))
        
        # Periodizing the signal
        window = np.hanning(len(result))
        result *= window
        
        # Computing the Fourier transform (and adding zeroes to the signal for accuracy)
        fft_result = abs((fft(result, n = len(result)*4)))
        kt = 2 * np.pi * fftfreq(len(fft_result), d= 10*dt)
        
        
        # Extraction of the pulsation
        
        return abs(kt[fft_result.argmax()])