#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 18:13:31 2017

@author: gee
"""
' BEWARE: your directory must be phy571_project '

import sys
sys.path.append('Design')
import numpy as np
import GPE_FFT_2D_Library as lib


# define the variables except eta
L = 10
J = 256
T = 200
N= 100001
N_0= 1000
V_1, V_2= 1,1

x_pts, dx = np.linspace(-L/2, L/2, J, retstep=True)
y_pts, dy = np.linspace(-L/2, L/2, J, retstep=True)
xx,yy = np.meshgrid(x_pts, y_pts)


eta_values = [-20, -10, -5, -2, -1, 1, 2, 5, 10, 20]

# we compute the groundstate for different values of eta
for eta in eta_values:
    lib.find_groundstate(L, J, T, N, N_0, V_1, V_2, eta, 'vortex')
    
    