#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 18:30:12 2017

@author: gee
"""
' BEWARE: your directory must be phy571_project '

import sys
sys.path.append('Design')
import numpy as np
import GPE_FFT_2D_Library as lib


# define the variables
L = 10
J = 256
T = 200
N= 100001
N_0= 1000
V_1, V_2= 1,1
eta= 1
x_pts, dx = np.linspace(-L/2, L/2, J, retstep=True)
y_pts, dy = np.linspace(-L/2, L/2, J, retstep=True)
xx,yy = np.meshgrid(x_pts, y_pts)

t_acquisition = 3000

eta_values = [-20, -5, -2, -1, 1, 2, 5,20]
xshift_values = [0.5, 1, 2]
a_values = [0.1, 0.5, 1, 2, 5, 10]


for eta in eta_values:
    groundstate = lib.load('Production/groundstate_result/vortex' + '_eta=' + str(eta))
    for xshift in xshift_values:
        for a in a_values:
            # add a vortex to the groundstate
            vortex = lib.add_vortex(groundstate, xx, yy, xshift, a)
            # normalize the new solution
            vortex =  vortex / np.sqrt((np.conjugate(vortex) * vortex ).sum()* dx * dy)
            # Compute the period
            lib.period(t_acquisition, vortex, xx, yy, lib.kxx, lib.kyy, dx, dy, lib.dt, V_1, V_2, eta, xshift, a)
            
    