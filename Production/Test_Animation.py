#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 20:00:24 2017

@author: gee
"""
' BEWARE: your directory must be phy571_project '

import sys
sys.path.append('Design')

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import GPE_FFT_2D_Library as lib



# parameters of the added vortex: horizontal shift and narrowness
xshift = 1
a = 1
    
# create the groundstate
#groundstate = lib.find_groundstate(lib.L, lib.J, lib.T, lib.N, 1000, lib.V_1, lib.V_2, -1*lib.eta, 'vortex')

# Load it
groundstate = lib.load('vortex')

# add a vortex 
vortex = lib.add_vortex(groundstate, -1*lib.xx, lib.yy, xshift, a)

'We can add a symetrical vortex'
vortex = lib.add_vortex(vortex, lib.xx, lib.yy, xshift, a)

# animate the vortex
lib.animate_vortex(vortex, lib.xx, lib.yy, lib.kxx, lib.kyy, lib.dt, lib.V_1, lib.V_2, lib.eta)
