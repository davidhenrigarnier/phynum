\select@language {english}
\contentsline {section}{Acknowledgements}{1}{section*.1}
\contentsline {section}{\numberline {1}Bose-Einstein condensates}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Bose-Einstein condensation}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}The Gross-Pitaevskii Equation}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Theory}{3}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Quasi-2D GPE}{4}{subsubsection.1.2.2}
\contentsline {subsection}{\numberline {1.3}Vortices in Bose-Einstein condensates}{5}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Vortex in a harmonically trapped BEC}{5}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}Methods for creation of a vortex in the condensate}{6}{subsubsection.1.3.2}
\contentsline {section}{\numberline {2}Numerical approach}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}The dimensionless GPE}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Integration of the GPE}{7}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Crank-Nicholson method}{7}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Slip step method}{8}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Finding the ground state: Imaginary time propagation}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Adding a vortex}{10}{subsection.2.4}
\contentsline {section}{\numberline {3}Results}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Early stages}{11}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Vortex results}{12}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}General results}{12}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Precession pulsation}{12}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Limit cases}{15}{subsubsection.3.2.3}
\contentsline {section}{Conclusion}{16}{section*.3}
\contentsline {section}{Bibliography}{17}{section*.4}
\contentsline {section}{Appendix}{18}{section*.5}
