\section{Numerical approach}
\label{p2}

\subsection{The dimensionless GPE}
To compute we need to have a dimensionless version of the Gross-Pitaevskii equation.

Here if $V_{ext}(x,y) = \frac{1}{2}m\omega^{2}(x^{2}+y^{2})$, we have

\begin{equation}
 i\hbar\frac{\partial\Psi}{\partial t} = \left\lbrack-\frac{\hbar^{2}}{2m}\left(\frac{\partial^{2}}{\partial x^{2}} + \frac{\partial^{2}}{\partial y^{2}}\right) + \frac{1}{2}m\omega^{2}(x^{2}+y^{2}) + g_{2D}N | \Psi |^{2}\right\rbrack \Psi(x, y, t)
\end{equation}

If we divide this equation by $\hbar\omega$, replace $\omega t \rightarrow t$, $x/x_{0} \rightarrow x $, and $\sqrt{x_{0}}\Psi \rightarrow \Psi$, then in natural units 

\begin{equation}
 i\hbar\frac{\partial\Psi}{\partial t} = \left\lbrack-\frac{1}{2}\left(\frac{\partial^{2}}{\partial x^{2}} + \frac{\partial^{2}}{\partial y^{2}}\right) + \frac{1}{2}(x^{2}+y^{2}) + \eta | \Psi |^{2}\right\rbrack \Psi(x, y, t) = (\hat{T}+\hat{V})\Psi=\hat{H}\Psi
\end{equation}

with dimensionless nonlinear coefficient

\begin{equation}
\eta = \frac{g_{1D}(N/x_{0})}{\hbar\omega}
\end{equation}

\subsection{Integration of the GPE}
\subsubsection{Crank-Nicholson method}

At first we tried the Cranck-Nicholson method. It's a classic way to solve time-dependent Schrodinger equations. The idea is to integrate directly the Scrodinger equation $i\partial\Psi / \partial t = H\Psi$ between $t$ and $t+\Delta t$ with $t = T/N$, $N\gg 1$. It leads to:

\begin{equation}
\Psi(\mathbf{r}, t + \Delta t) = \exp(-i\Delta t H)\Psi(\mathbf{r}, t) + O(\Delta t^{2})=\frac{1-\frac{i\Delta t}{2}H}{1+\frac{i\Delta t}{2}H}\Psi(\mathbf{r}, t) + O(\Delta t^{2})
\end{equation}
by using the Cayley's form.

By writing the wavefunction after $n$ time intervals as $\Psi(\mathbf{r},n\Delta t) = \Psi^{n}(\mathbf{r})$ the time evolution step can be written as

\begin{equation}
\left[1-\frac{i\Delta t}{2} H^{n+1}(\mathbf{r})\right]\Psi^{n+1}(\mathbf{r}) = \left[1+\frac{i\Delta t}{2} H^{n}(\mathbf{r})\right]\Psi^{n}(\mathbf{r})
\end{equation}
where $H^{n}(\mathbf{r})$ is the dimensionless GP hamiltonian at the $n^{th}$ time step, $H^{n}(\mathbf{r}) = -(1/2)\nabla^{2} + (1/2)\mathbf{r}^{2} + |\Psi^{n}(\mathbf{r})|^{2}$

The aim is to calculate $\Psi^{n+1}(\mathbf{r})$. By setting $H^{n+1}(\mathbf{r}) = H^{n}(\mathbf{r})$ and substitute this back into the original equation we calculate an intermediate solution and we iterate te process. To numerically solve the equation we have to discretise the wavefunction onto a spatial grid with grid spacing $\Delta x$. This equation is stable if $\Delta t < \Delta x^{2}/2$.

We left this method for the slip step method which is more convenient because it is hard to manage the boundaries condition with the Crank-Nicholson process and it is difficult to scale.

\subsubsection{Slip step method}
We rewrite the Eq. (13) with the Dirac notation:

\begin{equation}
 i\hbar\frac{\partial}{\partial t}|\Psi(t)\rangle = (\hat{T}+\hat{V})|\Psi(t) \rangle =\hat{H} |\Psi(t)\rangle
\end{equation}

 for initial condition $|\Psi(0)\rangle$ the solution is
 
 \begin{equation}
|\Psi(t)\rangle = \hat{U}(t) |\Psi(0)\rangle = e^{-i(\hat{T}+\hat{V})t}|\Psi(0)\rangle 
\end{equation}
 
To proceed we employ the Trotter product formula for the evolution operator $\hat{U}$
\begin{eqnarray*}
    \hat{U}(t) & = &  e^{-i(\hat{T}+\hat{V})t} \\
    & =  & \lim_{N\to\infty} \left[e^{-i\hat{T}(t/N)}e^{-i\hat{V}(t/N)} \right]^{N}\\
    & =  & \lim_{N\to\infty} \left[e^{-i\hat{T}\Delta t} e^{-i\hat{V}\Delta t} \right]^{N}\\
    & = & \lim_{N\to\infty} \left[\hat{U}(\Delta t)\right]^{N}
\end{eqnarray*}
where $\Delta t = t/N$.
As we apply for a small step $\Delta t$ we have:
 \begin{equation}
|\Psi(t+\Delta t)\rangle \approx e^{-i\hat{T}\Delta t}e^{-i\hat{V}\Delta t}|\Psi(t)\rangle 
\end{equation}
     
\begin{itemize}
\item $\hat{T}$ propagation
\end{itemize}
Propagation over a small time step  $\Delta t$ under the action of the kinetic energy operator $\hat{T}$ alone is easly performed in Fourier space:
\begin{equation}
\Psi(k_{x},t +\Delta t) = e^{-ik_{x}^{2}\Delta t/2}\Psi(k_{x},t +\Delta t) 
\end{equation}
by using the Fast Fourier Transform and the Inverse Fast Fourier Transform we find:
\begin{equation}
\Psi(x,t +\Delta t) = IFFT( e^{-ik_{x}^{2}\Delta t/2}FFT(\Psi(x,t)))
\end{equation}
and the criterion $|\Delta t| < \Delta x^{2}$ ensures that a small step under the action of the kinetic energy operator alone does not produce a large effect.

\begin{itemize}
\item $\hat{V}$ propagation
\end{itemize}
This step is particularly easy to implement knowing that
\begin{equation}
\Psi(x,t +\Delta t) = e^{-iV(x)\Delta t}\Psi(x,t) 
\end{equation}
and we require that $|V(x) \Delta t| < 1$ for all $x$ to ensure that the small step under the action of the potential energy operator alone does not make a large change.

Bringing everything together we find that in our quasi-2D GPE case we have:
\begin{equation}
\Psi(x, y, t +\Delta t) = e^{-iV(x,y)\Delta t} IFFT2( e^{-i(k_{x}^{2}+k_{y}^{2})\Delta t/2}FFT2(\Psi(x,y,t)))
\end{equation}


\subsection{Finding the ground state: Imaginary time propagation}
An easy way to obtain the ground state is by using propagation in imaginary time. Consider the wavefunction as a superposition of eigenstates $\phi_{m}(\mathbf{r})$ with time-dependent amplitudes $a_{m}(t)$ and eigenenergies $E_{m}(t)$, i.e. $\psi(\mathbf{r}, t) =  \sum_{m} a_{m}(t)\phi_{m}(\mathbf{r})$. By making the substitution $\Delta t \rightarrow -i\Delta t$, the time evolution equation leads to an exponential decay of the wavefunction, and a corresponding decay of the eigenstates via
\begin{equation}
\psi(\mathbf{r}, t + \Delta t) =  \sum_{m} a_{m}(t)\phi_{m}(\mathbf{r}) \exp(-E_{m}\Delta t)
\end{equation}
The eigenenergy governs the decay rate, and so the eigenstate with the lowest energy, the ground state of the system, decays slowest.


\subsection{Adding a vortex}
A vortex state in a 2D system, with rotational charge q, is generated by propagating the GPE in imaginary time, subject to a $2\pi q$ azimuthal phase slip. For example, to create a displaced singly-charged vortex, the wavefunction should be modified via
\begin{equation}
\psi \rightarrow |\psi| \exp[i \tan^{-1}(y/(x-x_{shift}))]
\end{equation}

