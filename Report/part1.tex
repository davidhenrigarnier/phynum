\section{Bose-Einstein condensates}

\subsection{Bose-Einstein condensation}

A Bose-Einstein condensate (BEC) is a gas of bosons which is condensed at very low temperature, in the limit where the temperature goes to zero. According to the laws of thermodynamics, all the particles from a gas behave the same manner and can occupy certain quantum states which correspond to certain energies. Whereas two fermions can't occupy the same quantum state because of the Pauli exclusion principle, any number of bosons can occupy the same quantum state. When a condensate of bosons is at the zero temperature, every particle occupies the ground state of the system.
	
Actually, before achieving this limit there are enough particles which occupy the ground state to give to the gas some very interesting macroscopic properties such as superfluidity or the creation of vortices in the system. 

Bose-Einstein condensates can be obtained experimentally by following several steps. First the atoms are cooled using lasers. Then they are moved to a magnetic trap. The last step of the process is the evaporative cooling which the idea is to keep only the coolest particles.

\subsection{The Gross-Pitaevskii Equation}
\subsubsection{Theory}

Bose-Einstein condensates are described by the Gross-Pitaevskii equation (GPE) of which a brief explaination is given here.

The Hamiltonian that describes the system is given by 

\begin{equation}
  \hat{H} = \sum_{i = 1}^{N} \left(\frac{\mathbf{p}_{i}^{2}}{2m} + V_{ext}(\mathbf{r}_{i})\right) + \frac{1}{2}\sum_{i = 1}^{N}\sum_{j \neq i}^{N} V(|\mathbf{r}_{i}-\mathbf{r}_{j}|)
\end{equation}

The terms on the right-hand-side are respectively the kinetic energy of each particle, the external potential $V_{ext}$ and the term of interactions between the $N$ particles.

Then the concept of thermodynamic potentials ensures that the ground state will be found by minimizing the free energy $F=E-\mu N$. In the condensate there are $N$ particles which are described by a wavefunction $\phi_{H}$. The Hartree approximation claims that the wavefunction is a product wave function in which all $N$ atoms are assumed to occupy the same time-dependent single-particle orbital :

\begin{equation}
  \Phi_{H}(\mathbf{r}_{1}, \mathbf{r}_{2}, . . ., \mathbf{r}_{N}, t)= \prod_{i = 1}^{N} \Psi(\mathbf{r}_{i},t)
  \end{equation}

This approximation is valid if the condensate is not very dense which is true for the condensates obtained experimentally.

Using that $E(\psi)=\frac{\langle \psi | \hat{H} | \psi \rangle}{\langle \psi | \psi \rangle}$, minimising the free energy leads to the following equation :

\begin{equation}
  -\frac{\hbar^{2}}{2m}\nabla^{2}\psi(\mathbf{r}) + V_{ext}(\mathbf{r})\psi(\mathbf{r}) + N\frac{4\pi\hbar^{2}}{m}a | \psi(\mathbf{r}) |^{2} \psi(\mathbf{r})= \mu \psi(\mathbf{r})
\end{equation}

which is the time independent Gross-Pitaevskii equation. $a$ indicates if the attractions between bosons are attractives ($a<0$) or repulsives ($a>0$).

Here the GPE which has been established is related to the stationary state of the condensate. Using the Schr$\ddot{o}$dinger equation it leads to the time-dependent Gross-Pitaevskii equation:

\begin{equation}
 i\hbar\frac{\partial\Psi}{\partial t} = -\frac{\hbar^{2}}{2m}\nabla^{2}\Psi + V_{ext}(\mathbf{r})\Psi + gN\ | \Psi |^{2} \Psi
\end{equation}

\subsubsection{Quasi-2D GPE}
Here we will compute the GPE in the 2D case.
We start from the 3D GPE in Cartesian coordinates:

\begin{equation}
 i\hbar\frac{\partial\Psi}{\partial t} = -\frac{\hbar^{2}}{2m}\left(\frac{\partial^{2}}{\partial x^{2}} + \frac{\partial^{2}}{\partial y^{2}} + \frac{\partial^{2}}{\partial z^{2}}\right)\Psi + V_{ext}(x, y, z)\Psi + g_{3D}N\ | \Psi |^{2} \Psi
\end{equation}

We assume a tight harmonic confinement along the $z$-axis compared to the transverse $(x, y)$ plane

\begin{equation}
V_{ext}(x, y, z) = \frac{1}{2}m\omega_{z}^{2}z^{2} + \tilde{V}_{ext}(x, y)
\end{equation}

with $\tilde{V}_{ext}(x, y)$ the effective one-particle potential in the transverse plane. In particular, we assume that the spatial structure along the $z$-axis is dominated by the single-particle potential and apply the quasi-2D ansatz

\begin{equation}
\Psi(x, y, z, t)= \tilde{\Psi}(x, y, t)\psi_{g}(z)e^{-\frac{iE_{g}t}{\hbar}}
\end{equation}

where $E_{g} = \hbar\omega_{z}/2$, and the ground state for the $z$-dependent portion of the single-particle potential obeys

\begin{equation}
E_{g}\psi_{g} = -\frac{\hbar^{2}}{2m}\frac{d^{2} \psi_{g}}{dz^{2}} + \frac{1}{2}m\omega_{z}^{2}z^{2}\psi_{g}(z)
\end{equation}

with every wave function normalized.

Then by substituting in the Eq. (5) we obtain the quasi-2D GPE that we will use:

\begin{equation}
 i\hbar\frac{\partial\tilde{\Psi}}{\partial t} = -\frac{\hbar^{2}}{2m}\left(\frac{\partial^{2}}{\partial x^{2}} + \frac{\partial^{2}}{\partial y^{2}}\right)\tilde{\Psi} + \tilde{V}_{ext}(x, y)\tilde{\Psi} + g_{2D}N\ | \tilde{\Psi} |^{2} \tilde{\Psi}(x, y, t)
\end{equation}

with the 2D coupling coefficient

\begin{equation}
g_{2D}=g_{3D}\frac{\int_{-\infty}^{\infty}dz|\psi_{g}(z)|^{4}}{\int_{-\infty}^{\infty}dz|\psi_{g}(z)|^{2}}
\end{equation}

For brevity in notation we shall hereafter drop the tilde over 2D quantities $ \tilde{\Psi} \rightarrow \Psi$, $\tilde{V}_{ext} \rightarrow V_{ext} $.



\subsection{Vortices in Bose-Einstein condensates}

After studying Bose-Einstein condensates and finding the ground state we will describe an aspect of the dynamics by studying vortices in BEC. 

\subsubsection{Vortex in a harmonically trapped BEC}

Dispite the fact that theory is complex, papers show that if one assume the harmonically trapped BEC as homogeneous (which is realistic in the quasi-2D GPE approximation which is satisfied if $\hbar\omega > \mu $ and $\omega_{z} \gg \omega$ so the BEC is almost homogenous in the $(x, y)$ plan) and one makes the Thomas-Fermi approximation (remove the kinetic term in the GPE, valid in the strongly repulsive limit), one can find that the vortex circles at the precession frequency:

\begin{equation}
\omega_{V} = \frac{\omega_{0}}{1-\frac{r^{2}}{R_{TF}^{2}}}
\end{equation}

with $\omega_{0} = \frac{3\hbar\omega^{2}}{4\mu}\ln\left(\frac{R_{TF}}{\xi}\right)$

where

\begin{itemize}
\item $R_{TF}=\sqrt{\frac{2\mu}{m\omega^{2}}}$ is the Thomas-Fermi radius
\item $r$ the length between the center of the trap and the center of the vortex
\item $\xi = \sqrt{\frac{\hbar^{2}}{2m|g|n}} $ the healing length
\item $\mu$ the chemical potential
\end{itemize}


\subsubsection{Methods for creation of a vortex in the condensate}

In practice there are several methods to create vortices in the condensate:

\begin{itemize}
\item by phase imprinting in the condensate
\item by rotating a gas of bosonic atoms while cooling them to reach the condensation temperature
\item by stirring a trap containing the condensate with an optical spoon 
\end{itemize}

At zero temperature, vortices can also appear by the presence of a velocity field from other vortices or fluid flow or a locally inhomogeneous density due to a confining potential